from kafka import KafkaProducer
import json

global_uid = 0

def register_user():
    # Kafka Producer Configuration
    producer = KafkaProducer(
        bootstrap_servers='kafka:9092',
        value_serializer=lambda v: json.dumps(v).encode('utf-8')
    )

    user_id = input("User ID to register: ")
    is_paid = input("Are you paid? (True/False): ")

    # User data to be registered
    user_data = {
        'user_id': user_id,
        'is_paid': bool(eval(is_paid)),  # Set this to True for paid users
        'register_user': True
    }

    print(user_data)
    global global_uid
    global_uid = user_id

    # Kafka topic for user registration
    user_registration_topic = 'users_topic'

    # Produce the message to the Kafka topic
    producer.send(user_registration_topic, value=user_data)

    # Close the producer
    producer.close()

def exit_program():
    print("Exiting the program.")
    exit()


def register_sales():
    # Kafka Producer Configuration
    producer = KafkaProducer(
        bootstrap_servers='kafka:9092',
        value_serializer=lambda v: json.dumps(v).encode('utf-8')
    )
    global global_uid
    user_id = global_uid
    num_sales = int(input("Number of Sales: "))
    total_sales = float(input("Total Sales Amount: "))

    # Message for sales
    sales_data = {
        'user_id': user_id,
        'num_sales': num_sales,
        'total_sales': total_sales
    }

    print(sales_data)

    # Kafka topic for sales
    sales_topic = 'sales_topic'

    # Produce the message to the Kafka topic
    producer.send(sales_topic, value=sales_data)

    # Close the producer
    producer.close()

def stock_alert():
    # Kafka Producer Configuration
    producer = KafkaProducer(
        bootstrap_servers='kafka:9092',
        value_serializer=lambda v: json.dumps(v).encode('utf-8')
    )

    global global_uid
    user_id = global_uid
    lacking_product = input("Lacking Product: ")

    # Message for stock check
    stock_data = {
        'user_id': user_id,
        'lacking_product': lacking_product
    }

    print(stock_data)

    # Kafka topic for stock alerts
    stock_topic = 'stock_topic'

    # Produce the message to the Kafka topic
    producer.send(stock_topic, value=stock_data)

    # Close the producer
    producer.close()


# Always start the registration of the user

register_user()

# Then, show different options to the registered user


# Switch menu dictionary
menu_options = {
    '1': stock_alert,
    '2': register_sales,
    '3': exit_program
}

while True:
    # Display the menu
    print("\nMenu:")
    print("1. Alert lack of stock")
    print("2. Register sales")
    print("3. Exit")

    # Get user input
    choice = input("Enter your choice (1-3): ")
        # Perform action based on user choice
    selected_option = menu_options.get(choice)
    if selected_option:
        selected_option()
    else:
        print("Invalid choice.")
