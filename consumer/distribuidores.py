from kafka import KafkaConsumer
import json
from datetime import datetime

# Kafka Consumer Configuration
consumer = KafkaConsumer(
    'stock_topic',
    bootstrap_servers='kafka:9092',
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
)

# JSON file to store stock alerts
json_file = 'stock_alerts.json'

# Main loop to consume stock messages
for stock_message in consumer:
    stock_data = stock_message.value

    # Extract information from the stock message
    user_id = stock_data['user_id']
    lacking_product = stock_data['lacking_product']

    # Get the current timestamp (date and hour)
    timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    # Create an alert message with timestamp
    alert_message = {
        'user_id': user_id,
        'product': lacking_product,
        'timestamp': timestamp
    }

    # Print the alert message in the CLI
    print(f"Low stock alert for seller {user_id}. Lacking product: {lacking_product}. Timestamp: {timestamp}")

    # Append the alert to the JSON file as a new entry
    with open(json_file, 'a') as file:
        file.write(json.dumps(alert_message) + '\n')

# Close the consumer
consumer.close()
