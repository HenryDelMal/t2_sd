from kafka import KafkaConsumer
import json

# Kafka Consumer Configuration
consumer = KafkaConsumer(
    'sales_topic',
    bootstrap_servers='kafka:9092',
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
)

# Dictionary to store total sales for each producer
sales_data_by_producer = {}

# Main loop to consume sales messages
for sales_message in consumer:
    sales_data = sales_message.value

    # Extract information from the sales message
    user_id = sales_data['user_id']
    num_sales = sales_data['num_sales']
    total_sales = sales_data['total_sales']

    # Update total sales for the producer
    sales_data_by_producer[user_id] = {
        'num_sales': sales_data_by_producer.get(user_id, {}).get('num_sales', 0) + num_sales,
        'total_sales': total_sales
    }

    # Print the sales information for each producer
    print(f"Sales for producer {user_id}: {num_sales} sales, Total sales: ${total_sales:.2f}")

    # Save the sales data to a JSON file
    with open('sales_data.json', 'w') as file:
        json.dump(sales_data_by_producer, file)

# Close the consumer
consumer.close()
