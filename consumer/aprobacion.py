from kafka import KafkaConsumer, KafkaProducer
import json

# Kafka Consumer Configuration
consumer = KafkaConsumer(
    'users_topic',
    bootstrap_servers='kafka:9092',
    value_deserializer=lambda x: json.loads(x.decode('utf-8'))
)

# Kafka Producer Configuration
response_producer = KafkaProducer(
    bootstrap_servers='kafka:9092',
    value_serializer=lambda v: json.dumps(v).encode('utf-8')
)

# Read the list of registered users from the JSON file
with open('registered_users.json', 'r') as file:
    registered_users = json.load(file)

# Main loop to consume messages
for message in consumer:
    user_data = message.value

    # Check if the user is already registered
    if user_data['user_id'] in [user['user_id'] for user in registered_users]:
        response_message = {
            'user_id': user_data['user_id'],
            'response': 'User already registered. Registration denied.'
        }
    else:
        # Process the message as needed
        print("Received message:", user_data)

        # Save user information to the list
        registered_users.append(user_data)

        # Save the updated list of registered users to the JSON file
        with open('registered_users.json', 'w') as file:
            json.dump(registered_users, file)

        response_message = {
            'user_id': user_data['user_id'],
            'response': 'Registration successful'
        }

    # Send a response to the response topic
    response_producer.send('response_topic', value=response_message)

# Close the consumer
consumer.close()
